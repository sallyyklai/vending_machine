import unittest
import vendingmachine as v

testmachine = v.vendingMachine(cola_stock = 5, choco_stock = 0, crisp_stock = 5, balance = 0,change = 0, nickels = 1, dimes= 2,quarters=2)

class v_m_testcase(unittest.TestCase):
    def SetUp(self):
        print('testing')
    def tearDown(self):
        print('lol testing')
    def test_machine_coin(self):
        testmachine.balance = 0
        self.assertEqual(testmachine.Take_coin(0.25),0.25)
        self.assertEqual(testmachine.Take_coin(0.05),0.30)
        self.assertEqual(testmachine.Take_coin(0.07),0.30)
    # def test_buy_product(self):
    #     testmachine.balance = 1
    #     self.assertEqual(testmachine.buy_product('cola',1),(0,4))
    def test_change_for_product(self):
        testmachine.balance = 1
        self.assertTrue(testmachine.change_for_product('crisp',1,0,0,4))
if __name__ == '__main__':
    unittest.main()
